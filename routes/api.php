<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/user', function (Request $request) {
//    return $request->user();
//})->middleware('auth:api');

Route::post('authenticate', 'Auth\AuthController@authenticate');
Route::post('log-out','Auth\AuthController@logOut');


Route::group(['prefix' => 'events', 'middleware' => ['jwt.auth']], function () {
    Route::get('/', 'EventsController@index');
    Route::post('/', 'EventsController@store');

    Route::get('/activities', 'EventsController@fetchActivities');

    Route::get('/public/{id}', 'EventsController@getPublicEvent');
    Route::post('/join-event', 'EventsController@joinEvent');

    Route::get('/own', 'EventsController@getOwnEvents');
    Route::get('/private/{id}', 'EventsController@getPrivateEvent');
    Route::post('/invite', 'EventsController@inviteUser');

    Route::post('/accept-request', 'EventsController@accept');

    Route::post('/search', 'EventsController@searchEvents');

    Route::get('/list-requests', 'EventsController@listRequests');

    Route::get('/list-invites', 'EventsController@listInvites');

    Route::get('/{id}/comments', 'EventsController@getComments');
    Route::post('/add-comment', 'EventsController@addComment');

    Route::post('/request-action', 'EventsController@requestAction');

    Route::post('/cancel-event', 'EventsController@cancelEvent');

});

Route::group(['prefix' => 'user', 'middleware' => ['jwt.auth']], function () {
    Route::get('/', 'UserController@getUsers');
    Route::get('/details', 'UserController@fetchCurrentLoggedIn');
    Route::post('/edit-details', 'UserController@editDetails');
});
