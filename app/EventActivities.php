<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class EventActivities extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    public function activityDetails()
    {
        return $this->hasOne('App\Activities','id','activity_id');
    }

    public function addActivities($eventID,$data) {
        $toInsert = [];

        foreach (explode(',',$data) as $activity) {
            array_push($toInsert,[
                'event_id' => $eventID,
                'activity_id' => $activity
            ]);
        }
//        return $toInsert;
        EventActivities::insert($toInsert);
    }

}
