<?php
/**
 * Created by PhpStorm.
 * User: jenarymadia
 * Date: 22/02/2017
 * Time: 10:46 PM
 */

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;
use App\User;

class AuthController extends Controller
{
    protected $jwtAuth;
    protected $registerController;
    public function __construct(JWTAuth $JWTAuth,RegisterController $registerController)
    {
        $this->jwtAuth = $JWTAuth;
        $this->registerController = $registerController;
    }

    public function authenticate(Request $request) {
        $credentials = [
            'email' => $request->email
        ];

        $user = User::where($credentials)->first();
        if(! $user) {
            $this->registerController->create([
                'name' => $request->name,
                'email' => $request->email,
                'profile_picture' => $request->profilePicture,
                'password' => $request->pid
            ]);
            $user = User::where($credentials)->first();
        }
        $token = $this->jwtAuth->fromUser($user);
        return response()->json([
            'success' => true,
            'message' => 'Login successful',
            'token' => compact('token')
        ],200);

    }



    public function logOut() {
        $this->jwtAuth->invalidate($this->jwtAuth->getToken());
    }
}