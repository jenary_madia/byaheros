<?php

namespace App\Http\Controllers;

use App\Activities;
use App\EventActivities;
use App\Events;
use App\Joiners;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    public function getUsers() {
        return User::select('id','email','name')->get();
    }

    public  function fetchCurrentLoggedIn() {
        if (Auth::user()->id) {
            return User::where("id",Auth::user()->id)
                ->select('id','email','name','profile_picture')->first();
        }
    }

    public function editDetails(Request $request) {

        DB::beginTransaction();
        try {
            $user = User::where("id",Auth::user()->id)->first();
            if($request->name) {
                $user->name = $request->name;
            }

            if($request->profilePicture) {
                $user->profile_picture = $request->file('profilePicture')->getClientOriginalName();
                $request->file('photo_url')->storeAs(
                    "user-photo", $request->file('profilePicture')->getClientOriginalName()
                );
            }

            $user->save();
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'User details successfully updated!'
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Please contact developers'
            ],400);
        }

    }

}
