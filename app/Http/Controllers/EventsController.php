<?php

namespace App\Http\Controllers;

use App\Activities;
use App\Comments;
use App\EventActivities;
use App\Events;
use App\Joiners;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\Exception;


class EventsController extends Controller
{
    protected $eventActivities;
    protected $joiners;
    protected $events;

    public function __construct(EventActivities $eventActivities,Joiners $joiners,Events $events)
    {
        $this->eventActivities = $eventActivities;
        $this->joiners = $joiners;
        $this->events = $events;

    }

    public function index() {
        return Events::fetchPublicEvents();
    }

    /**
     * @param Request $request
     * @return string
     */
    public function store(Request $request) {
        $rules = [
            'name' => 'required|unique:events,name',
            'location' => 'required',
            'price' => 'required|numeric|min:1',
            'photo_url' => 'required | image',
            'details' => 'required',
            'start_date_time' => 'required|date_format:"Y-m-d H:i:s"',
            'end_date_time' => 'required|date_format:"Y-m-d H:i:s"',
            'accessibility' => 'required|numeric',
            'max_participants' => 'required|numeric|min:3'
        ];
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => "Something went wrong",
            ],400); // 400 being the HTTP code for an invalid request.
        }

        DB::beginTransaction();
        try {
            $event = Events::create([
                'organizer' => Auth::user()->id,
                'name' => $request->name,
                'location' => $request->location,
                'price' => $request->price,
                'photo_url' => $request->file('photo_url')->getClientOriginalName(),
                'details' => $request->details,
                'start_date_time' => $request->start_date_time,
                'end_date_time' => $request->end_date_time,
                'accessibility' => $request->accessibility,
                'max_participants' => $request->max_participants,
                'popularity' => $request->popularity,
            ]);
            $this->eventActivities->addActivities($event->id,$request->activity);

            $request->file('photo_url')->storeAs(
                "events-photo", $request->file('photo_url')->getClientOriginalName()
            );

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'Event added!'
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Please contact developers'
            ],400);

            // return response()->json([
            //     'success' => false,
            //     'message' => $e->getMessage()
            // ],500);
        }
    }

    public function getPublicEvent($id) {
        $event = Events::fetchEvent($id,1);
        if(! $event) {
            return response()->json([
                'success' => false,
                'message' => "Invalid event",
            ],400); // 400 being the HTTP code for an invalid request.
        }
        $status = Joiners::where([
            'event_id' => $id,
            'joiner_id' => Auth::user()->id
        ])->first();

        $event['status'] = $status ? $status->approved : 3;

        return $event;
    }

    public function getPrivateEvent($id) {
        $event = Events::fetchEvent($id,2);
        if(! $event) {
            return response()->json([
                'success' => false,
                'message' => "Invalid event",
            ],400);
        }
        $status = Joiners::where([
            'event_id' => $id,
            'joiner_id' => Auth::user()->id
        ])->first();
        $event['status'] = $status ? $status->approved : 3;

        return $event;
    }

    public function getOwnEvents() {
        return Events::fetchOwnEvents();
    }

    public function joinEvent(Request $request) {
        $rules = [
            'eventID' => 'required',
        ];
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => "Something went wrong",
            ],400); // 400 being the HTTP code for an invalid request.
        }
        DB::beginTransaction();
        try {
            $publicEvent = Events::where([
                'id' => $request->eventID,
                'accessibility' => 1
            ])->first();

            $alreadyRequested = Joiners::where([
                'event_id' => $request->eventID,
                'joiner_id' => Auth::user()->id,
                'approved' => 0
            ])->first();

            $alreadyJoined = Joiners::where([
                'event_id' => $request->eventID,
                'joiner_id' => Auth::user()->id,
                'approved' => 1
            ])->first();

            if( ! $publicEvent) {
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid Event'
                ],400);
            }

            if($alreadyJoined) {
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'message' => 'The user already joined on this event'
                ],400);
            }

            if($alreadyRequested) {
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'message' => 'The user already sent a request to join on this event'
                ],400);
            }

            Joiners::create([
                'event_id' => $request->eventID,
                'joiner_id' => Auth::user()->id,
            ]);

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "Successfully sent request to join $publicEvent->name"
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Please contact developers'
            ],400);

            // return response()->json([
            //     'success' => false,
            //     'message' => $e->getMessage()
            // ],500);
        }
    }

    public function accept(Request $request) {
        $rules = [
            'requestID' => 'required',
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => "Something went wrong",
            ],400); // 400 being the HTTP code for an invalid request.
        }
        try {
            DB::beginTransaction();
            $request = Joiners::find($request->requestID);
            if(! $request) {
                DB::commit();
                return response()->json([
                    'success' => false,
                    'message' => "Something went wrong",
                ],400); // 400 being the HTTP code for an invalid request.
            }
            if($request->approved == 0) {
                $message = "Joiner accepted!";
            }else{
                $message = "Invite accepted!";
            }

            $request->approved = 1;
            $request->save();

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => $message
            ],200);


        }catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Please contact developers'
            ],400);
        }
    }

    public function inviteUser(Request $request) {
        $rules = [
            'eventID' => 'required',
            'userID' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => "Something went wrong",
            ],400); // 400 being the HTTP code for an invalid request.
        }
        DB::beginTransaction();
        try {
            $publicEvent = Events::where([
                'id' => $request->eventID,
                'accessibility' => 2,
                'organizer' => Auth::user()->id
            ])->first();

            $toInvite = User::where([
                'id' => $request->userID
            ])->first();

            $joiners = Joiners::where([
                'event_id' => $request->eventID,
                'joiner_id' => $request->userID
            ])
            ->whereIn('approved',[0,2])
            ->count();

            $alreadyRequested = Joiners::where([
                'event_id' => $request->eventID,
                'joiner_id' => $request->userID,
                'approved' => 2
            ])->first();

            $alreadyJoined = Joiners::where([
                'event_id' => $request->eventID,
                'joiner_id' => $request->userID,
                'approved' => 1
            ])->first();

            if( ! $publicEvent) {
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid Event'
                ],400);
            }

            if($joiners > $publicEvent->max_participants) {
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'message' => 'Maximum joiners reached!'
                ],400);
            }

            if( ! $toInvite) {
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid user'
                ],400);
            }

            if($alreadyJoined) {
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'message' => 'The user already joined on this event'
                ],400);
            }

            if($alreadyRequested) {
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'message' => 'You already sent invite to the user on this event'
                ],400);
            }

            Joiners::create([
                'event_id' => $request->eventID,
                'joiner_id' => $request->userID,
                'approved' => 2
            ]);

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "Successfully invited!"
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Please contact developers'
            ],400);
        }
    }

    public function searchEvents(Request $request) {

        $interest = $request->interest ? explode(",",$request->interest) : null;

        Return $this->events->search(
            $request->name,
            $interest,
            $request->budgetFrom,
            $request->budgetTo,
            $request->date,
            $request->duration,
            $request->recent,
            $request->popular
        );
    }

    public function listRequests() {
        return $this->joiners->fetchJoiners();
    }

    public function listInvites() {
        return $this->joiners->fetchInvites();
    }

    public function fetchActivities() {
        return Activities::all();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getComments($id) {
        $event = Events::find($id);
        if(! $event) {
            return response()->json([
                'success' => false,
                'message' => "Invalid event",
            ],400); // 400 being the HTTP code for an invalid request.
        }

        return Comments::where([
            'event_id' => $id
        ])->with('commentorDetails')
        ->orderBy('id','desc')
        ->get();
    }

    public function addComment(Request $request) {
        $rules = [
            'eventID' => 'required',
            'comment' => 'required',
        ];
        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => "Something went wrong",
            ],400); // 400 being the HTTP code for an invalid request.
        }
        DB::beginTransaction();
        try {
            $publicEvent = Events::where([
                'id' => $request->eventID,
                'accessibility' => 1
            ])->first();

            if( ! $publicEvent) {
                DB::rollback();
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid Event'
                ],400);
            }

            Comments::create([
                'event_id' => $request->eventID,
                'comment' => $request->comment,
                'commentor_id' => Auth::user()->id,
            ]);

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "Comment sucessfully posted"
            ],200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Please contact developers'
            ],400);
        }

    }

    public function requestAction(Request $request) {
        /*
         * 1 = cancelled
         * 2 = unjoin
         * 3 = decline
         * */
        $rules = [
            'eventID' => 'required',
            'action' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => "Something went wrong",
            ],400); // 400 being the HTTP code for an invalid request.
        }
        try {
            DB::beginTransaction();
            switch ($request->action) {
                case 1 :
                    $message = "Request cancelled";
                    break;
                case 2 :
                    $message = "You've leave the event";
                    break;
//                case 3 :
//                    $message = "Request declined";
//                    break;

            }

            $request = Joiners::where([
                'event_id' => $request->eventID,
                'joiner_id' => Auth::user()->id
            ])->first();

            if(! $request) {
                DB::commit();
                return response()->json([
                    'success' => false,
                    'message' => "Invalid joiner details",
                ],400); // 400 being the HTTP code for an invalid request.
            }

            $request->delete();

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => $message
            ],200);


        }catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Please contact developers'
            ],400);
        }
    }

    public function cancelEvent(Request $request) {
        $rules = [
            'eventID' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => "Something went wrong",
            ],400); // 400 being the HTTP code for an invalid request.
        }
        try {
            DB::beginTransaction();

            $request = Events::where([
                'id' => $request->eventID,
                'organizer' => Auth::user()->id
            ])->first();

            if(! $request) {
                DB::commit();
                return response()->json([
                    'success' => false,
                    'message' => "Invalid event",
                ],400); // 400 being the HTTP code for an invalid request.
            }

            $request->delete();
            EventActivities::where('event_id',$request->eventID)->delete();
            Joiners::where('event_id',$request->eventID)->delete();

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "Event successfully cancelled"
            ],200);


        }catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Please contact developers'
            ],400);
        }
    }
}
