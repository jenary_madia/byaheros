<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function commentorDetails() {
        return $this->hasOne('App\User','id','commentor_id')->select('id','name','email','profile_picture');
    }

}
