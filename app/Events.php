<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Events extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public static function fetchPublicEvents() {
        return Events::where('accessibility',1)
            ->with('activities')
            ->with('organizer')
            ->orderBy('id','desc')
            ->get();
    }

    public static function fetchEvent($id,$accessibility) {
        return Events::where([
            'id' => $id,
            'accessibility' => $accessibility
        ])->with('activities')
            ->with('organizer')
            ->with('joiners')
            ->orderBy('id','desc')
            ->first();

    }

    public static function fetchOwnEvents() {
        return Events::where([
            'organizer' => Auth::user()->id
        ])->with('activities')
            ->with('organizer')
            ->with('joiners')
            ->orderBy('id','desc')
            ->get();
    }

    public function activities()
    {
        return $this->hasMany('App\EventActivities','event_id','id')->with('activityDetails');
    }

    public function organizer()
    {
        return $this->hasOne('App\User','id','organizer')->select('id','name','email','profile_picture');
    }

    public function joiners()
    {
        return $this->hasMany('App\Joiners','event_id','id')->with('joinerDetails');
    }

    public function comments()
    {
        return $this->hasMany('App\Comments','event_id','id')->with('commentorDetails');
    }

    public function search(
        $name = null,
        $interest = null,
        $budgetFrom = null,
        $budgetTo = null,
        $date = null,
        $duration = null,
        $recent = null,
        $popular = null
    )
    {

        $events = Events::join('event_activities', 'event_activities.event_id', '=', 'events.id')
        ->leftjoin(DB::raw("(select event_id,max(id) as jCount from joiners) joiners"), 'joiners.event_id', '=', 'events.id')
        ->where('accessibility' , 1)
        ->with("organizer");
        if($name) {
            $events->where('name','LIKE', "%$name%");
        }

        if($interest) {
            $events->whereIn('event_activities.activity_id', $interest);
        }

        if($budgetFrom && $budgetTo) {
            $events->whereRaw("price BETWEEN '$budgetFrom' and '$budgetTo'");
        }

        if($date) {
            $date = date('Y-m-d G:i:s', strtotime($date));
            $events->whereRaw("start_date_time = '$date'");
        }
        if($duration) {
            $events->whereRaw("datediff(start_date_time,end_date_time) = $duration");
        }

        if($recent) {
            $events->orderBy('created_at', 'desc');
        }
//
//        if($popular) {
//            $events->orderBy('jCount', 'desc');
//        }
        $events = $events->select('events.*')
        ->groupBy("events.name")->get();

        return $events;
    }
}
