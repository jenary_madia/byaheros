<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Joiners extends Model
{
    protected $guarded = [];

    public function joinerDetails() {
        return $this->hasOne('App\User','id','joiner_id')->select('id','name','email','profile_picture');
    }

    public function eventDetails() {
        return $this->hasOne('App\Event','id','joiner_id')->select('id','name');
    }

    public function fetchJoiners() {
        return DB::table('joiners')
            ->join('events', 'joiners.event_id', '=', 'events.id')
            ->join('users', 'joiners.joiner_id', '=', 'users.id')
            ->where('events.organizer',Auth::user()->id )
            ->where('joiners.approved',0 )
            ->select('joiners.id as request_id','events.id as event_id','events.name as event_name','users.name as user_name')
            ->get();
    }

    public function fetchInvites() {
        return DB::table('joiners')
            ->join('events', 'joiners.event_id', '=', 'events.id')
            ->join('users', 'joiners.joiner_id', '=', 'users.id')
            ->where('joiners.joiner_id',Auth::user()->id )
            ->where('joiners.approved',2 )
            ->select('joiners.id as request_id','events.id as event_id','events.name as event_name','users.name as user_name')
            ->get();
    }
}
