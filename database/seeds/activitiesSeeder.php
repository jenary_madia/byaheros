<?php

use Illuminate\Database\Seeder;
use App\Activities;

class activitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Activities::insert([
            ['name' => 'TRAVEL'],
            ['name' => 'HIKE'],
            ['name' => 'SWIM'],
            ['name' => 'EAT'],
            ['name' => 'PARTY'],
            ['name' => 'COFFEE'],
            ['name' => 'MOVIE'],
            ['name' => 'JOG'],
            ['name' => 'DRINK'],
            ['name' => 'JAM'],
            ['name' => 'CONCERT'],
            ['name' => 'BIKE'],
            ['name' => 'GYM'],
            ['name' => 'BASKETBALL'],
            ['name' => 'SURF'],
            ['name' => 'COOK'],
            ['name' => 'BAKE'],
            ['name' => 'PHOTOGRAPHY'],
            ['name' => 'VIDEO GAME'],
            ['name' => 'SHOP'],
            ['name' => 'VOLUNTEER WORKS']
        ]);
    }
}
