<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->text('photo_url')->nullable()->change();
            $table->renameColumn('start_date_ime', 'start_date_time');
            $table->renameColumn('end_date_ime', 'end_date_time');
            $table->dateTime('start_date_ime')->nullable()->change();
            $table->dateTime('end_date_ime')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            //
        });
    }
}
